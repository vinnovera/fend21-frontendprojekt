const myform = document.getElementById('myform');

const inputs = myform.querySelectorAll('input');

const passInput = myform.querySelector('input[type="password"]');

passInput.style.display = 'inline';

passInput.addEventListener('focus', () => { 
  console.log('In focus');
});

passInput.addEventListener('blur', () => { 
  console.log('Out of focus');
});

const rangeInput = myform.querySelector('input[type="range"]');

rangeInput.addEventListener('change', () => { 
  console.log('input change');
});

passInput.addEventListener('change', () => { 
  console.log('input change');
});

const myotherbutton = document.getElementById('myotherbutton');
myotherbutton.addEventListener('click', evt => { 
  console.log(evt);
});

myform.addEventListener('submit', e => { 
  e.preventDefault();

  const termsBox = document.getElementById('accept');
  const checkedAge = document.querySelector('input[name="age"]:checked');

  console.log(passInput.value);
  console.log(rangeInput.value);
  console.log(termsBox.checked);

  if (checkedAge) { 
    console.log(checkedAge.value);
  }

  const radioBtns = document.querySelectorAll('input[name="age"]');
  let checkedBtn = null;
  radioBtns.forEach(rbtn => { 
    if (rbtn.checked) { 
      checkedBtn = rbtn;
    } 
  });

  if (checkedBtn) { 
    console.log(checkedBtn.value);
  }
  
});

