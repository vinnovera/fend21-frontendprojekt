const userform = document.getElementById('userform');
const nameField = document.getElementById('name');

nameField.addEventListener('input', () => { 
  if (!nameField.classList.contains('interactinginput')) { 
    nameField.classList.add('interactinginput');
  }

  if (nameField.validity.tooShort) {
    nameField.setCustomValidity('Måste vara långt namn');
  } else { 
    nameField.setCustomValidity('');
  }
});

userform.addEventListener('submit', (e) => {
  e.preventDefault();

  console.log(nameField.validity);
});
