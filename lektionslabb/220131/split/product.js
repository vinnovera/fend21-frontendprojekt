// Self executing function, to create a scope to avoid global scope pollution
// Function returns a function (bottom of scope), 
// that is assigned to global variable "drawProduct"
const drawProduct = (function () {
  // Default options, if none are provided
  const defaults = {
    header: '',
    desc: '',
    image: null
  }

  // Draw container, not in global scope
  const drawContainer = () => { 
    const container = document.createElement('article');
    return container;
  }

  // Draws a header, not in global scope
  const drawHeader = (txt) => { 
    const h2 = document.createElement('h2');
    h2.innerText = txt;
    return h2;
  }

  // Draws description, not in global scope
  const drawDesc = (txt) => { 
    const p = document.createElement('p');
    p.innerText = txt;
    return p;
  }

  // Draws the image, not in global scope
  const drawImg = (src) => { 
    const img = document.createElement('img');
    img.src = src;
    return img;
  }

  // This function is returned by the self executing function
  // This is what gets assigned to "drawProduct" in global scope
  // and is available for everyone
  return (opts) => { 
    // Use spread operator to combine defaults with parameter options
    opts = {
      ...defaults,
      ...opts
    };
    
    // Draw all elements
    const container = drawContainer();
    container.appendChild(drawHeader(opts.header));
    container.appendChild(drawDesc(opts.desc));
    if (opts.image) { 
      container.appendChild(drawImg(opts.image));
    }

    // Return container with all the elements
    return container;
  }
})();
