const chatbox = document.querySelector('#chatbox');
const showChatBtn = document.querySelector('#showChatBtn');

chatbox.style.display = 'none';

showChatBtn.addEventListener('click', function () {
  if (chatbox.style.display === 'block') {
    chatbox.style.display = 'none';
  } else { 
    chatbox.style.display = 'block';
  }
});
