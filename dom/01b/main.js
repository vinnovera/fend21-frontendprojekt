const chatbox = document.querySelector('#chatbox');
const showChatBtn = document.querySelector('#showChatBtn');

showChatBtn.addEventListener('click', function () {
  if (chatbox.classList.contains('hidden')) {
    chatbox.classList.remove('hidden');
  } else { 
    chatbox.classList.add('hidden');
  }
});
