const messages = [
  {
    from: 'bot',
    text: 'Välkommen till vår chat! Kan jag hjälpa till med något?'
  },
  {
    from: 'user',
    text: 'Något går fel när jag försöker bekräfta beställningen'
  },
  {
    from: 'bot',
    text: 'Det verkar som du inte klickat i "godkänn"-rutan'
  }
];

const chatbox = document.querySelector('#chatbox');
const showChatBtn = document.querySelector('#showChatBtn');
const messagelist = document.querySelector('#messagelist')

showChatBtn.addEventListener('click', function () {
  if (chatbox.classList.contains('hidden')) {
    chatbox.classList.remove('hidden');
  } else { 
    chatbox.classList.add('hidden');
  }
});

function createMessageNode(message) { 
  const item = document.createElement('li');
  item.innerText = message.text;
  item.classList.add(message.from);
  return item;
}

messages.forEach(function(message) {
  messagelist.appendChild(createMessageNode(message));
});
