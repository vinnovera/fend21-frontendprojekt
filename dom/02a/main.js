const messages = [
  'Välkommen till vår chat! Kan jag hjälpa till med något?',
  'Något går fel när jag försöker bekräfta beställningen',
  'Det verkar som du inte klickat i "godkänn"-rutan'
];

const chatbox = document.querySelector('#chatbox');
const showChatBtn = document.querySelector('#showChatBtn');
const messagelist = document.querySelector('#messagelist')

showChatBtn.addEventListener('click', function () {
  if (chatbox.classList.contains('hidden')) {
    chatbox.classList.remove('hidden');
  } else { 
    chatbox.classList.add('hidden');
  }
});

messages.forEach(function(message) {
  messagelist.innerHTML += message + '<br>';
});
