const messages = [
  {
    from: 'bot',
    text: 'Välkommen till vår chat! Kan jag hjälpa till med något?'
  },
  {
    from: 'user',
    text: 'Något går fel när jag försöker bekräfta beställningen'
  },
  {
    from: 'bot',
    text: 'Det verkar som du inte klickat i "godkänn"-rutan'
  }
];

const chatbox = document.querySelector('#chatbox');
const showChatBtn = document.querySelector('#showChatBtn');
const messagelist = document.querySelector('#messagelist');
const messageTemplate = (message) => `
  <li class="${message.from}">
    ${message.text}
  </li>
`;

showChatBtn.addEventListener('click', function () {
  if (chatbox.classList.contains('hidden')) {
    chatbox.classList.remove('hidden');
  } else { 
    chatbox.classList.add('hidden');
  }
});

messagelist.innerHTML = messages.map(messageTemplate).join('');
