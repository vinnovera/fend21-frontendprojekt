const area = document.getElementById('area');
const clear = document.getElementById('clear');

area.value = localStorage.getItem('area');
area.addEventListener('input', () => {
  localStorage.setItem('area', area.value);
});

clear.addEventListener('click', () => {
  localStorage.removeItem('area');
  area.value = '';
});
