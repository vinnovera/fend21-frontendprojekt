const userform = document.getElementById('userform');
const userdetails = document.getElementById('userdetails');
const nameInput = document.getElementById('name');
const emailInput = document.getElementById('email');
const usernameField = document.getElementById('username');
const useremailField = document.getElementById('useremail');

const userObj = JSON.parse(localStorage.getItem('user'));

if (userObj) {
  userdetails.classList.remove('hidden');
  userform.classList.add('hidden');

  usernameField.innerText = userObj.name;
  useremailField.innerText = userObj.email;
}

userform.addEventListener('submit', function (e) {
  e.preventDefault();

  if (nameInput.value !== '' && emailInput.value !== '') {
    localStorage.setItem('user', JSON.stringify({
      name: nameInput.value,
      email: emailInput.value
    }));

    location.reload();
  } else { 
    alert('Fyll i namn och epost');
  }
});
