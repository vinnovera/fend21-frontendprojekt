const SVGUtils = {
  drawSVG: function (id = 'svg', width = 300, height = 300) {
    const svgElem = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
    svgElem.setAttribute('id', id);
    svgElem.setAttribute('width', width);
    svgElem.setAttribute('height', height);
    svgElem.setAttribute('viewBox', `0 0 ${width} ${height}`);
    return svgElem;
  },

  drawCircle: function(width, strokeWidth, style) {
    const circleElem = document.createElementNS('http://www.w3.org/2000/svg', 'circle');
    circleElem.setAttribute('cx', width / 2);
    circleElem.setAttribute('cy', width / 2);
    circleElem.setAttribute('r', (width / 2) - (strokeWidth / 2));
    circleElem.setAttribute('stroke-width', strokeWidth);
    Object.keys(style).map(p => ({ prop: p, value: style[p] })).forEach(({prop, value}) => {
      circleElem.setAttribute(prop, value);
    });
    return circleElem;
  },

  drawText: function (text, style) {
    const textElem = document.createElementNS('http://www.w3.org/2000/svg', 'text');
    Object.keys(style).map(p => ({ prop: p, value: style[p] })).forEach(({prop, value}) => {
      textElem.setAttribute(prop, value);
    });
    textElem.innerHTML = text;
    return textElem;
  }
}
