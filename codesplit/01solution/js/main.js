const list = document.createElement('ul');
document.body.appendChild(list);

function drawStatistic(stat) { 
  const wrapper = document.createElement('div');
  const title = document.createElement('h3');
  const donut = Donut({ progress: stat.percentage / 100, width: 130 });
  wrapper.appendChild(title);
  wrapper.appendChild(donut);
  wrapper.style.textAlign = 'center';
  wrapper.style.marginBottom = '4em';
  title.innerText = stat.title;
  title.style.fontFamily = 'Arial';
  title.style.color = 'hsl(180deg, 75%, 20%)';
  return wrapper;
}

statistics.forEach(function (stat) { 
  const li = drawStatistic(stat);
  list.appendChild(li);
});
