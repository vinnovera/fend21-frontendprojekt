const Donut = (function () {
  const defaultDonutOptions = {
    id: 'donut',
    width: 200,
    progress: 0,
    strokeWidth: 6,
    backgroundStyle: {
      stroke: 'hsl(180deg, 75%, 20%)',
      fill: 'none',
    },
    mainStyle: {
      stroke: 'hsl(180deg, 75%, 55%)',
      fill: 'none',
    },
    textStyle: {
      'font-family': 'Arial',
      'font-size': '48px',
      fill: 'hsl(180deg, 75%, 55%)',
    }
  }

  function drawDonut(_opts) {
    const opts = Object.assign(defaultDonutOptions, _opts);
    const {
      id,
      width,
      progress,
      strokeWidth,
      backgroundStyle,
      mainStyle,
      textStyle
    } = opts;

    const svg = SVGUtils.drawSVG(id, width, width);
    const circumference = width * Math.PI - (strokeWidth * Math.PI);

    const bgcircle = SVGUtils.drawCircle(width, strokeWidth, backgroundStyle);
    svg.appendChild(bgcircle);
    
    const circle = SVGUtils.drawCircle(width, strokeWidth, {
      ...mainStyle,
      'stroke-dasharray': circumference,
      'stroke-dashoffset': circumference - (circumference * progress),
      transform: `rotate(-90) translate(-${width} 0)`
    });
    svg.appendChild(circle);

    const text = SVGUtils.drawText(progress * 100 + '%', {
      ...textStyle,
      dy: '0.32em',
      'text-anchor': 'middle',
      transform: `translate(${width / 2} ${width / 2})`
    });
    svg.appendChild(text);

    return svg;
  }

  return drawDonut;
})();
