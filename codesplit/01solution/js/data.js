const statistics = [
  {
    title: 'How many adults sleep with comfort objects (toys)',
    percentage: 33
  },
  {
    title: 'How many people wash their hands after using the bathroom',
    percentage: 20
  },
  {
    title: 'The percent of worldwide military in top 5 countries',
    percentage: 60
  },
  {
    title: 'How many people over 65 use Snapchat',
    percentage: 3
  },
];