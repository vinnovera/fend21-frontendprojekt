function drawSVG(id = 'svg', width = 300, height = 300) {
  const svgElem = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
  svgElem.setAttribute('id', id);
  svgElem.setAttribute('width', width);
  svgElem.setAttribute('height', height);
  svgElem.setAttribute('viewBox', `0 0 ${width} ${height}`);
  return svgElem;
}

function drawCircle(width, strokeWidth, style) {
  const circleElem = document.createElementNS('http://www.w3.org/2000/svg', 'circle');
  circleElem.setAttribute('cx', width / 2);
  circleElem.setAttribute('cy', width / 2);
  circleElem.setAttribute('r', (width / 2) - (strokeWidth / 2));
  circleElem.setAttribute('stroke-width', strokeWidth);
  Object.keys(style).map(p => ({ prop: p, value: style[p] })).forEach(({prop, value}) => {
    circleElem.setAttribute(prop, value);
  });
  return circleElem;
}

function drawText(text, style) {
  const textElem = document.createElementNS('http://www.w3.org/2000/svg', 'text');
  Object.keys(style).map(p => ({ prop: p, value: style[p] })).forEach(({prop, value}) => {
    textElem.setAttribute(prop, value);
  });
  textElem.innerHTML = text;
  return textElem;
}

const defaultDonutOptions = {
  id: 'donut',
  width: 200,
  progress: 0,
  strokeWidth: 6,
  backgroundStyle: {
    stroke: 'hsl(180deg, 75%, 20%)',
    fill: 'none',
  },
  mainStyle: {
    stroke: 'hsl(180deg, 75%, 55%)',
    fill: 'none',
  },
  textStyle: {
    'font-family': 'Arial',
    'font-size': '48px',
    fill: 'hsl(180deg, 75%, 55%)',
  }
}

function drawDonut(_opts) {
  const opts = Object.assign(defaultDonutOptions, _opts);
  const {
    id,
    width,
    progress,
    strokeWidth,
    backgroundStyle,
    mainStyle,
    textStyle
  } = opts;

  const svg = drawSVG(id, width, width);
  const circumference = width * Math.PI - (strokeWidth * Math.PI);

  const bgcircle = drawCircle(width, strokeWidth, backgroundStyle);
  svg.appendChild(bgcircle);
  
  const circle = drawCircle(width, strokeWidth, {
    ...mainStyle,
    'stroke-dasharray': circumference,
    'stroke-dashoffset': circumference - (circumference * progress),
    transform: `rotate(-90) translate(-${ width } 0)`
  });
  svg.appendChild(circle);

  const text = drawText(progress * 100 + '%', {
    ...textStyle,
    dy: '0.32em',
    'text-anchor': 'middle',
    transform: `translate(${width / 2} ${width / 2})`
  });
  svg.appendChild(text);

  return svg;
}

function drawStatistic(stat) { 
  const wrapper = document.createElement('div');
  const title = document.createElement('h3');
  const donut = drawDonut({ progress: stat.percentage / 100, width: 130 });
  wrapper.appendChild(title);
  wrapper.appendChild(donut);
  wrapper.style.textAlign = 'center';
  wrapper.style.marginBottom = '4em';
  title.innerText = stat.title;
  title.style.fontFamily = 'Arial';
  title.style.color = 'hsl(180deg, 75%, 20%)';
  return wrapper;
}

const statistics = [
  {
    title: 'How many adults sleep with comfort objects (toys)',
    percentage: 33
  },
  {
    title: 'How many people wash their hands after using the bathroom',
    percentage: 20
  },
  {
    title: 'The percent of worldwide military in top 5 countries',
    percentage: 60
  },
  {
    title: 'How many people over 65 use Snapchat',
    percentage: 3
  },
];


const list = document.createElement('ul');
document.body.appendChild(list);

statistics.forEach(function (stat) { 
  const li = drawStatistic(stat);
  list.appendChild(li);
});
