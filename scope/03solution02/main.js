function showHelp(help) {
  document.getElementById('help').textContent = help;
}

function makeCallback(help) { 
  return function() {
    showHelp(help);
  };
}

function setupHelp() {
  var helpText = [
      {'id': 'email', 'help': 'Din e-mail adress'},
      {'id': 'name', 'help': 'Ditt namn'},
      {'id': 'age', 'help': 'Din ålder (du måste vara över 18)'}
    ];

  for (var i = 0; i < helpText.length; i++) {
    var item = helpText[i];
    document.getElementById(item.id).onfocus = makeCallback(item.help);
  }
}

setupHelp();
