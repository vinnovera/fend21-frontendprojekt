/*
function delay(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}
*/

/*
function delay(ms) { 
  return new Promise(function(resolve, reject) { 
    setTimeout(function () { 
      resolve();
    }, ms);
  });
}
*/

const delay = ms => new Promise(r => setTimeout(r, ms));

delay(3000).then(() => alert('runs after 3 seconds'));
