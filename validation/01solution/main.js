const servicename = document.getElementById('servicename');
servicename.addEventListener('input', () => {
  if (servicename.validity.tooShort) {
    servicename.setCustomValidity('Tjänstens namn måste vara minst fem tecken långt');
  } else { 
    servicename.setCustomValidity("");
  }
});
