const getNewsBtn = document.getElementById('getNews');
const newsItemsList = document.getElementById('newsItems');

getNewsBtn.addEventListener('click', async function () { 
  const response = await fetch('./news.json');
  const data = await response.json();
  
  data.news.forEach(function(item) {
    const li = document.createElement('li');
    li.innerHTML = `
      <h2>${item.headline}</h2>
      <p>${item.content}</p>
    `;
    newsItemsList.appendChild(li);
  });
});
