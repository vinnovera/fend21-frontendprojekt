const menuoptionsForm = document.getElementById('menuoptions');

menuoptionsForm.addEventListener('submit', (e) => {
  e.preventDefault();

  const checked = document.querySelectorAll('input[type="checkbox"]:checked');
  let toppings = '';
  checked.forEach((box) => {
    toppings += box.value + '\n';
  });

  alert(toppings);
});
