const kitchenField = document.getElementById('kitchen');
const bathroomField = document.getElementById('bathroom');
const roomSelect = document.getElementById('room');

kitchenField.style.display = 'none';
bathroomField.style.display = 'none';

roomSelect.addEventListener('change', () => { 
  kitchenField.style.display = 'none';
  bathroomField.style.display = 'none';

  switch (roomSelect.value) {
    case 'kitchen':
      kitchenField.style.display = 'inherit';
      break;
    case 'bathroom':
      bathroomField.style.display = 'inherit';
      break;
  }
});