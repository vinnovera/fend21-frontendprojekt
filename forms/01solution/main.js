const loginform = document.getElementById('loginform');
const welcomeScreen = document.getElementById('welcome');
const wrongScreen = document.getElementById('wrong');

const USERNAME = 'admin';
const PASSWORD = 'pass123';

welcomeScreen.style.display = 'none';
wrongScreen.style.display = 'none';

loginform.addEventListener('submit', (e) => {
  e.preventDefault();

  const userField = document.getElementById('username');
  const passField = document.getElementById('password');

  if (userField.value === USERNAME && passField.value === PASSWORD) {
    wrongScreen.style.display = 'none';
    welcomeScreen.style.display = 'inherit';
  } else { 
    welcomeScreen.style.display = 'none';
    wrongScreen.style.display = 'inherit';
  }
});
