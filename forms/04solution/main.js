const pages = {
  about: document.getElementById('about'),
  products: document.getElementById('products'),
  shipping: document.getElementById('shipping')
}
const tabmenu = document.getElementById('tabmenu');
const tabs = tabmenu.querySelectorAll('input[type="radio"]');

let currentPage = 'about';

console.log(pages[currentPage]);

function showCurrentPage() { 
  pages.about.style.display = 'none';
  pages.products.style.display = 'none';
  pages.shipping.style.display = 'none';

  pages[currentPage].style.display = 'inherit';
}

tabs.forEach(tab => {
  tab.addEventListener('change', (e) => {
    currentPage = e.target.value;
    showCurrentPage();
  });
});

showCurrentPage();

